# app2

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).  

1.完成首页、城市列表页  
1/20/2018 9:43:47 AM 
2.完成个人中心页  
1/21/2018 9:00:34 AM   
3.完成订单详情及vip里面其它模块静态页面  
1/24/2018 9:22:39 AM   
4.完成日历插件、添加了线上直采、线下采购、品牌征集页面  
2/5/2018 1:49:38 PM 