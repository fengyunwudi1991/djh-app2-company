import Vue from 'vue'
import Router from 'vue-router'
//首页
const Home = r => require.ensure([], () => r(require('@/pages/index')), 'Home')
const checkCity = r => require.ensure([], () => r(require('@/pages/checkCity/index')), 'checkCity')
const founder = r => require.ensure([], () => r(require('@/pages/founder/index')), 'founder')
const register = r => require.ensure([], () => r(require('@/pages/boundPhone/register')), 'register')
//会员中心
const vip = r => require.ensure([], () => r(require('@/pages/vip/index')), 'vip')
const vipMain = r => require.ensure([], () => r(require('@/pages/vip/main')), 'vipMain')
const vipInfo = r => require.ensure([], () => r(require('@/pages/vip/vipInfo')), 'vipInfo')
const saveName = r => require.ensure([], () => r(require('@/pages/vip/saveName')), 'saveName')
//我的大件会
const myHeader = r => require.ensure([], () => r(require('@/pages/vip/order/myHeader')), 'myHeader')
const signRecord = r => require.ensure([], () => r(require('@/pages/vip/order/signRecord')), 'signRecord')
const qdRecord = r => require.ensure([], () => r(require('@/pages/vip/order/qdRecord')), 'qdRecord')
//我的订单
const order = r => require.ensure([], () => r(require('@/pages/vip/order/order')), 'order')
const allOrder = r => require.ensure([], () => r(require('@/pages/vip/order/index')), 'allOrder')
const dfkOrder = r => require.ensure([], () => r(require('@/pages/vip/order/dfkOrder')), 'dfkOrder')
const dshOrder = r => require.ensure([], () => r(require('@/pages/vip/order/dshOrder')), 'dshOrder')
const ywcOrder = r => require.ensure([], () => r(require('@/pages/vip/order/ywcOrder')), 'ywcOrder')
//订单详情
const confirmDetail = r => require.ensure([], () => r(require('@/pages/vip/order/confirmDetail')), 'confirmDetail')
const dfkdeail = r => require.ensure([], () => r(require('@/pages/vip/order/dfkdeail')), 'dfkdeail')
const successPay = r => require.ensure([], () => r(require('@/pages/vip/order/successPay')), 'successPay')
const restDeail = r => require.ensure([], () => r(require('@/pages/vip/order/restDeail')), 'restDeail')
const getDetail = r => require.ensure([], () => r(require('@/pages/vip/order/getDetail')), 'getDetail')
const nocommitDetail = r => require.ensure([], () => r(require('@/pages/vip/order/nocommitDetail')), 'nocommitDetail')
const checkcommit = r => require.ensure([], () => r(require('@/pages/vip/order/checkcommit')), 'checkcommit')
const commitPage = r => require.ensure([], () => r(require('@/pages/vip/order/commitPage')), 'commitPage')
const commitCont = r => require.ensure([], () => r(require('@/pages/vip/order/commitCont')), 'commitCont')
//日历
const calendar = r => require.ensure([], () => r(require('@/pages/canlerdar/App')), 'calendar')
//详情页
const collectApply = r => require.ensure([], () => r(require('@/pages/info/collectApply')), 'collectApply')
const pinpaiCollect = r => require.ensure([], () => r(require('@/pages/info/pinpaiCollect')), 'pinpaiCollect')
const collectDetail = r => require.ensure([], () => r(require('@/pages/info/collectDetail')), 'collectDetail')
Router.prototype.goBack = function () {
	this.isBack = true
	window.history.go(-1) //返回方法
}
Vue.use(Router)
export default new Router({
	mode: 'history',
	linkActiveClass: 'active',
	linkExactActiveClass: 'on',
	routes: [{
		path: '/',
		name: 'Home',
		component: Home,
		meta: {
			title: '大件会'
		}
	},
		/* {
			path: '/',
			name: 'calendar',
			component: calendar,
			meta: {
				title: '活动日历'
			}
		}, */
		{
			path: '/info/collectApply',
			name: 'collectApply',
			component: collectApply,
			meta: {
				title: '线下直采'
			}
		},
		{
			path: '/info/pinpaiCollect',
			name: 'pinpaiCollect',
			component: pinpaiCollect,
			meta: {
				title: '品牌征集'
			}
		},
		{
			path: '/collectDetail',
			name: 'collectDetail',
			component: collectDetail,
			meta: {
				title: '线上直采'
			}
		},
		{
			path: '/checkCity',
			name: 'checkCity',
			component: checkCity,
			meta: {
				title: '城市切换'
			}
		},
		{
			path: '/vip',
			name: 'vip',
			component: vip,
			meta: {
				title: '个人中心'
			},
			children: [
				{
					path: '/',
					// name: 'vipInfo',
					component: vipMain,
					meta: {
						title: '个人中心',
						keepAlive: true
					}
				},
				{
					path: 'vipInfo',
					name: 'vipInfo',
					component: vipInfo,
					meta: {
						title: '个人信息'
					}
				},
				{
					path: 'saveName',
					name: 'saveName',
					component: saveName,
					meta: {
						title: '保存昵称'
					}
				},
				{
					path: 'order',
					component: order,
					meta: {
						title: '我的拼团'
					},
					children: [
						{
							path: 'allOrder',
							name: 'allOrder',
							component: allOrder,
							meta: {
								title: '全部订单',
							}
						},
						{
							path: 'dfkOrder',
							name: 'dfkOrder',
							component: dfkOrder,
							meta: {
								title: '代付款'
							}
						},
						{
							path: 'dshOrder',
							name: 'dshOrder',
							component: dshOrder,
							meta: {
								title: '待收货'
							}
						}
						,
						{
							path: 'ywcOrder',
							name: 'ywcOrder',
							component: ywcOrder,
							meta: {
								title: '完成'
							}
						}
					]
				},
				{
					path: 'order/confirmDetail',
					name: 'confirmDetail',
					component: confirmDetail,
					meta: {
						title: '确认订单详情',
						keepAlive: true
					}
				},
				{
					path: 'order/dfkdeail',
					name: 'dfkdeail',
					component: dfkdeail,
					meta: {
						title: '支付订单详情',
						keepAlive: true
					}
				},
				{
					path: 'order/restDeail',
					name: 'restDeail',
					component: restDeail,
					meta: {
						title: '余款支付',
						keepAlive: true
					}
				},
				{
					path: 'order/getDetail',
					name: 'getDetail',
					component: getDetail,
					meta: {
						title: '收货',
						keepAlive: true
					}
				},
				{
					path: 'order/nocommitDetail',
					name: 'nocommitDetail',
					component: nocommitDetail,
					meta: {
						title: '待评价',
						keepAlive: true
					}
				},
				{
					path: 'order/commitPage',
					name: 'commitPage',
					component: commitPage,
					meta: {
						title: '待评价',
						keepAlive: true
					}
				},
				{
					path: 'order/checkcommit',
					name: 'checkcommit',
					component: checkcommit,
					meta: {
						title: '查看评价',
						keepAlive: true
					}
				},
				{
					path: 'order/commitCont',
					name: 'commitCont',
					component: commitCont,
					meta: {
						title: '评价内容',
						keepAlive: true
					}
				},
				{
					path: 'order/successPay',
					name: 'successPay',
					component: successPay,
					meta: {
						title: '支付成功'
					}
				},
				{
					path: 'order/myHeader',
					name: 'myHeader',
					component: myHeader,
					meta: {
						title: '我的团长'
					}
				},
				{
					path: 'order/signRecord',
					name: 'signRecord',
					component: signRecord,
					meta: {
						title: '报名记录'
					}
				}
				,
				{
					path: 'order/qdRecord',
					name: 'qdRecord',
					component: qdRecord,
					meta: {
						title: '签到记录',
					}
				}

			]
		},

		{
			path: '/register',
			name: 'register',
			component: register,
			meta: {
				title: '注册账号'
			}
		},
		{
			path: '/calendar',
			name: 'calendar',
			component: calendar,
			meta: {
				title: '日历'
			}
		}
	],
	mode: 'history'
})
