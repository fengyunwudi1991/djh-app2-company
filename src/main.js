// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import router from './router'
import VueCookie from 'vue-cookie'
import Mint from 'mint-ui'
//日历
import vueEventCalendar from './assets/js'

import Navigation from 'vue-navigation'
import { api } from './config'

import { is_weixin } from './config'

Vue.use(vueEventCalendar, { locale: 'zh', weekStartOn: 1 })
//点击延迟处理

//if ('addEventListener' in document) {
//  document.addEventListener('DOMContentLoaded', function() {
//      FastClick.attach(document.body)
//  }, false);
//}
let setDocumentTitle = function(title) {
	document.title = title;
	let ua = navigator.userAgent;
	if(/\bMicroMessenger\/([\d\.]+)/.test(ua) && /ip(hone|od|ad)/i.test(ua)) {
		var i = document.createElement('iframe');
		i.src = '/static/images/favicon.ico';
		i.style.display = 'none';
		i.onload = function() {
			setTimeout(function() {
				i.remove();
			}, 0);
		};
		document.body.appendChild(i);
	}
};
router.beforeEach((to, from, next) => {
	(typeof(to.meta.pageTitle !== 'undefined')) && setDocumentTitle(to.meta.title);
	if(to.meta.requireAuth) { // 判断该路由是否需要登录权限
		if(localStorage.getItem('token')) { // 通过vuex state获取当前的token是否存在
			next();
		} else {
			if(is_weixin()) {
				next({
					path: '/wecateLogin',
					query: {
						redirect: to.fullPath
					} // 将跳转的路由path作为参数，登录成功后跳转到该路由
				})
			} else {
				next({
					path: '/smsLogin',
					query: {
						redirect: to.fullPath
					} // 将跳转的路由path作为参数，登录成功后跳转到该路由
				})
			}
		}
	} else {
		next();
	}
})

//图片

Vue.filter('apiurl', function(value) {
	if(!value) {
		return ''
	}

	return api + value
})

Vue.filter('uppercase', function(value) {
	if(!value) {
		return ''
	}
	value = value.toString()
	return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.config.productionTip = false
Vue.use(VueCookie)
Vue.use(VueRouter)
Vue.use(Mint);
Vue.use(Navigation, {
	router
})


import 'mint-ui/lib/style.css'
// import './assets/css/common.css'
import './assets/css/common.less'
import './assets/css/style.less'
// import './assets/css/commonInfo.css'
// import './assets/css/collectDetail.css'

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	template: '<App/>',
	components: {
		App
	}
})