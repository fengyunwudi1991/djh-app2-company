export const messageBox = function(yestext, notext, text, callback) {
	
	var html = '<div class="msgBox">' +
		'<div class="msgicon"></div>' +
		'<div class="msgtext">' + text + '</div>' +
		'<div class="msgbtn wbox">' +
		'<div class="wfl"><button type="button" class="no">' + notext + '</button></div>' +
		'<div class="wfl"><button type="button" class="yes">' + yestext + '</button></div>' +
		'</div>' +
		'</div>' +
		'</div>'

	var a = document.createElement('div');
	a.setAttribute('class', 'msgmask')
	a.innerHTML = html
	document.body.appendChild(a)
	var yes = document.getElementsByClassName('yes')[0]
	var no = document.getElementsByClassName('no')[0]
	var msgBox = document.getElementsByClassName('msgmask')[0]
	yes.onclick = function() {
		if(callback && typeof callback === 'function') {
			callback()
			msgBox.remove()
		}
	}
	no.onclick = function() {
		msgBox.remove()
	}

}