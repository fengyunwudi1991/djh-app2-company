export const api = 'http://api.cs.dajianhui.com'
// export const api = 'http://api.dajianhui.com'
//export const api = 'http://192.168.100.58:9090'
export const api1 = 'http://m.wh.cs.dajianhui.com'
export const wxCallback = api1 + '/wx/wxCallback'

//export const api='http://api1.buy.dajianhui.com'
//export const api = 'http://api.dj17.com'

//加密

var CryptoJS = require("crypto-js");
var key = "db0bf515044b1bf3" //秘钥必须为：8/16/32位
export const encrypting = function(message) {
	var encrypts = CryptoJS.AES.encrypt(message, CryptoJS.enc.Utf8.parse(key), {
		mode: CryptoJS.mode.ECB,
		padding: CryptoJS.pad.Pkcs7
	});
	let b = "ciphertext=" + encrypts
	return b
}

//安卓还是ios判断
export const isIos = function() {
	var ua = navigator.userAgent.toLowerCase();
	if(ua.indexOf('micromessenger') != -1) {
		if(ua.indexOf('iphone') != -1 || ua.indexOf('ipad') != -1 || ua.indexOf('ipod') != -1) {
			return 'ios'
		} else {
			return 'android'
		}
	}
}

export const IsPC = function() {
  var userAgentInfo = navigator.userAgent;
  var Agents = ["Android", "iPhone",
        "SymbianOS", "Windows Phone",
        "iPad", "iPod"];
  var flag = true;
  for (var v = 0; v < Agents.length; v++) {
    if (userAgentInfo.indexOf(Agents[v]) > 0) {
      flag = false;
      break;
    }
  }
  return flag;
}

//埋点

export const statistics = function(EventId, Label, MapKv) {
	let cityInfo = JSON.parse(localStorage.getItem('cityInfo'))
	let cityName
	if(cityInfo) {
		cityName = cityInfo.cityName
	} else {
		cityName = '武汉'
	}
	let city = {
		"城市": cityName
	}
	let kv = Object.assign(city, MapKv)
	TDAPP.onEvent(EventId, Label, kv)
}

//过滤表情

export const isEmojiCharacter = function(substring) {
	for(var i = 0; i < substring.length; i++) {
		var hs = substring.charCodeAt(i);
		if(0xd800 <= hs && hs <= 0xdbff) {
			if(substring.length > 1) {
				var ls = substring.charCodeAt(i + 1);
				var uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
				if(0x1d000 <= uc && uc <= 0x1f77f) {
					return true;
				}
			}
		} else if(substring.length > 1) {
			var ls = substring.charCodeAt(i + 1);
			if(ls == 0x20e3) {
				return true;
			}
		} else {
			if(0x2100 <= hs && hs <= 0x27ff) {
				return true;
			} else if(0x2B05 <= hs && hs <= 0x2b07) {
				return true;
			} else if(0x2934 <= hs && hs <= 0x2935) {
				return true;
			} else if(0x3297 <= hs && hs <= 0x3299) {
				return true;
			} else if(hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 ||
				hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b ||
				hs == 0x2b50) {
				return true;
			}
		}
	}
}

//格式话日期
export const formatDateTime = function(date) {
	if(data){
	let y = date.getFullYear();
	let m = date.getMonth() + 1;
	m = m < 10 ? ('0' + m) : m;
	let d = date.getDate();
	d = d < 10 ? ('0' + d) : d;
	let h = date.getHours();
	let minute = date.getMinutes();
	minute = minute < 10 ? ('0' + minute) : minute;
	return y + '-' + m + '-' + d + ' ' + h + ':' + minute;
	}
};

//多种格式化日期

export const formatDate = function(date, type) {
	// console.log(type)
	let y = date.getFullYear();
	let m = date.getMonth() + 1;
	m = m < 10 ? '0' + m : m;
	let d = date.getDate();
	d = d < 10 ? ('0' + d) : d;
	let h = date.getHours();
	h = h < 10 ? ('0' + h) : h;
	let minute = date.getMinutes();
	minute = minute < 10 ? ('0' + minute) : minute;
	let second = date.getSeconds();
	second = second < 10 ? ('0' + second) : second;
	if(type === 2) {
		return y + '-' + m + '-' + d ;
	} else if(type === 3) {
		return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
	} else {
		return y + '.' + m + '.' + d;
	}
};

//日期转换

export const getDate = function(tm, type) {
	var tt = new Date(tm * 1000)
	tt = formatDate(tt, type)
	return tt;
}

// 数字分割
export const formatMoney = function(num) {
	var num = (num || 0).toString(),
		result = '';
	while(num.length > 3) {
		result = ',' + num.slice(-3) + result;
		num = num.slice(0, num.length - 3);
	}
	if(num) {
		result = num + result;
	}
	return result;
}

export const is_weixin = function() {
	var ua = window.navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i) == 'micromessenger') {
		return true
	} else {
		return false
	}
}